# Notes for Potential Speakers

We want #wd42 events to be welcoming for speakers of all ages and levels of experience. We especially encourage you to speak at our events if you're a first-timer, new to the field, or an [impostor](https://en.wikipedia.org/wiki/Impostor_syndrome).

## Who should speak
Anyone who is involved in software, websites, apps, frameworks, libraries, tools, or programming languages. Your role might be in planning, speccing, designing, building, reviewing, maintaining, teaching, documenting, testing, copywriting, or project-managing.

## What should they speak about
You can talk about whatever interests you. We've had a variety of talks, including ones about:
- Programming languages
- Tools, frameworks, libraries
- Case studies of projects people have worked on
- Writing for web content
- Graphic design and UX
- Team building
- Mentoring and teaching
- Practices and processes
- Software testing
- Marketing products

We love new and interesting topics and talk formats so if you have an idea let's chat!

## I've got an idea what do I do?
If you have an idea brewing you should definitely reach out to one of the organisers. We can have a chat and organise an event slot for you.

## Talk length
We try to have regular talks go for 25 - 30 minutes. This is generally a good length and leaves 10 - 15 minutes for questions, chat, and overrun.
We sometimes run Lightning Talks. This is when we put 2 or 3 talks of 5 - 10 minutes into one speaking slot. These are more light-weight talks for smaller subjects with less time for questions, etc. Some topics might be better suited for lightning talks rather than stretching to fill a full slot.

## Do I need to come to the events to speak?
We encourage anyone in Hobart who is interested in this field to attend our events. We prefer speakers to have attended at least one event before they speak, but it's not a requirement.
